Jonathan Clark Individual Evaluation
=========

Ansible script to deploy Httpd docker container on AWS ec2 instance (RHEL9)


Requirements
------------


Devcontainer environment requirements: Python3, PIP, Ansible, Open ssh



You will need and inventory file for connecting to aws host. It should look like the diagram below

    webserver  ansible_host=*aws-public-ip*   ansible_user=ec2-user  ansible_ssh_private_key_file=*path/to/ssh-key.pem*


File structure
------------

You will need to have a custom html page to replace the default. To achieve this, create a Jinja template for the file. It should be located in the file structure below.  You will also need a secure private key file for access to your aws ec2 instance. Note: the keys directory will be added to the gitignore file so sensitive data isn't uploaded to a public repository. 


```                                  
  
ansible
 ┣ inventory
 ┃ ┗ inventory.txt
 ┣ keys
 ┃ ┗ test-key1.pem <---------- * private key file
 ┣ roles
 ┃ ┗ httpd
 ┃ ┃ ┣ tasks
 ┃ ┃ ┃ ┣ 1-installDocker.yml
 ┃ ┃ ┃ ┣ 2-deployHttpd.yml
 ┃ ┃ ┃ ┗ main.yml
 ┃ ┃ ┣ templates
 ┃ ┃ ┃ ┗ index.html.j2 <----------- *  index.html template file
 ┃ ┃ ┗ vars
 ┃ ┃ ┃ ┗ main.yml
 ┣ root
 ┃ ┗ webpage
 ┃ ┃ ┗ index.html.j2
 ┗ site.yml                                       

```

Example Playbook
----------------

To execute you will need a playbook like the one listed below in your site.yml file.

    ---

    - hosts: webserver
      gather_facts: true
      roles:
        - httpd



Running the playbook
=====================

Run the following command to run ansible locally within .devcontainer:

```
ansible-playbook -vvv -i ansible/inventory/inventory.txt ansible/site.yml
```



Author
------------------
Jonathan Clark 